
//
// https://reactnavigation.org/docs/params
//
import * as React from 'react'
import { View, Text, TextInput, Image, Button } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

function LogoTitle() {
  return (
    <>
      <Image
      style={{ width: 25, height: 25 }}
      source={require('./assets/favicon.png')}
      />
      <Text> Logo</Text>
    </>
  )
}

function CreatePostScreen({ navigation, route }) {
  const [postText, setPostText] = React.useState('');
  return (
    <>
      <TextInput
        multiline
        placeholder="What's on your mind?"
        style={{ height: 200, padding: 10, backgroundColor: 'white' }}
        value={postText}
        onChangeText={setPostText}
      />
      <Button
        title="Done"
        onPress={() => {
          // Pass and merge params back to home screen
          navigation.navigate({
            name: 'Start',
            params: { post: postText },
            merge: true,
          });
        }}
      />
      <Button
        title="Update the title"
        onPress={() => navigation.setOptions({ title: 'Updated!' })}
      />
    </>
  );
}



function HomeScreen({ navigation, extraData }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
      <Text>{extraData.data}</Text>
      <Button
        title="Navigate to Details"
        onPress={() => navigation.navigate('Details',
          {
            itemId: 42,
            otherParam: 'anything you want here'
          }
        )}
      />
      <Button
        title="Navigate to Start"
        onPress={() => navigation.navigate('Start',
          {
            itemId: 1,
            otherParam: 'Start!!!'
          }
        )}
      />
    </View>
  )
}


function StartScreen({ navigation, route }) {
  React.useEffect(() => {
    if (route.params?.post) {
      // Post updated, do something with `route.params.post`
      // For example, send the post to the server
    }
  }, [route.params?.post]);

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        title="Create post"
        onPress={() => navigation.navigate('CreatePost')}
      />
      <Text style={{ margin: 10 }}>Post: {route.params?.post}</Text>
    </View>
  );
}


function DetailsScreen({ route, navigation }) { 
  if (!route.params) {
    return (
      <View>
        <Text>Empty</Text>
      </View>
    )
  }
  const { itemId, otherParam } = route.params
  return (    
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>
      <Text>itemId: {JSON.stringify(itemId)}</Text>
      <Text>itemId: {route.params.itemId}</Text>
      <Text>itemId: {itemId}</Text>
      <Text>otherParam: {JSON.stringify(otherParam)}</Text>      
      <Text>otherParam: {route.params.otherParam}</Text>            
      <Text>otherParam: {otherParam}</Text>            
      <Button
        title="Navigate to Details... again"
        onPress={() => navigation.navigate('Details')}
      />
      <Button
        title="Push to Details..."
        onPress={() => navigation.push('Details', {
          itemId: Math.floor(Math.random() * 100),
        })}
      />
      <Button title="Go to Home screen" onPress={() => navigation.navigate('Home')} />
      <Button title="Go back" onPress={() => navigation.goBack()} />
      <Button
        title="Go back to first screen in stack"
        onPress={() => navigation.popToTop()}
      />
      <Button title="Go to Count screen" onPress={() => navigation.navigate('Count')} />
    </View>
  )
}
/*
function StackScreen() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ title: 'My home' }}
      />
      <Stack.Screen
        name="Profile"
        component={ProfileScreen}
        options={({ route }) => ({ title: route.params.name })}
      />
    </Stack.Navigator>
  )
}
*/


function CountScreen({ navigation }) {
  const [count, setCount] = React.useState(0);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button onPress={() => setCount(c => c + 1)} title="Update count" />
      ),
    });
  }, [navigation]);

  return <Text>Count: {count}</Text>;
}


const Stack = createNativeStackNavigator()

const someData = {data: "Hello at home screen"}
function App() {
  const [count, setCount] = React.useState(0);
  // screenOptions - для всех окон
  // screenOptions={{
  //   headerStyle: {
  //   backgroundColor: '#f4511e',
  // },
  // headerTintColor: '#fff',
  // headerTitleStyle: {
  //   fontWeight: 'bold',
  // },
  // }}
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
      >
        <Stack.Screen name="Home"
          options={{
            headerTitle: (props) => <LogoTitle {...props} />,
            headerRight: () => (
              <Button
                onPress={() => alert('This is a button!')}
                title="Info"
              />
            )
          }}
        >
          {props => <HomeScreen {...props} extraData={someData} />}
        </Stack.Screen>
        <Stack.Screen
          name="Details"
          component={DetailsScreen}
          options={{
            title: 'Overview title',
            headerStyle: {
            backgroundColor: '#f4511e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
          initialParams={{  itemId: 42,
                            otherParam: 'anything you want here' }}>
        </Stack.Screen>
        <Stack.Screen name="Start">
          {props => <StartScreen {...props} extraData={someData} />}
        </Stack.Screen>
        <Stack.Screen name="CreatePost">
          {props => <CreatePostScreen {...props} extraData={someData} />}
        </Stack.Screen>
        <Stack.Screen name="Count">
          {props => <CountScreen {...props} extraData={someData} />}
        </Stack.Screen>

      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App
